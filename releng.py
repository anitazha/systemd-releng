#!/usr/bin/env python3

import argparse
import logging
import re
import subprocess
import sys
from datetime import datetime
from pathlib import Path
from typing import Any, NoReturn, Optional, Sequence, Iterator
import enum
import tempfile
import os
import contextlib

SYSTEMD_REPO = "https://github.com/systemd/systemd"
RPM_REPO = "https://git.centos.org/rpms/systemd.git"
AUTHOR = "CentOS Hyperscale SIG <centos-devel@centos.org>"


class LogFormatter(logging.Formatter):
    def __init__(self, fmt: Optional[str] = None, *args: Any, **kwargs: Any) -> None:
        fmt = fmt or "%(message)s"

        bold = "\033[0;1;39m" if sys.stderr.isatty() else ""
        gray = "\x1b[38;20m" if sys.stderr.isatty() else ""
        red = "\033[31;1m" if sys.stderr.isatty() else ""
        yellow = "\033[33;1m" if sys.stderr.isatty() else ""
        reset = "\033[0m" if sys.stderr.isatty() else ""

        self.formatters = {
            logging.DEBUG: logging.Formatter(f"‣ {gray}{fmt}{reset}"),
            logging.INFO: logging.Formatter(f"‣ {fmt}"),
            logging.WARNING: logging.Formatter(f"‣ {yellow}{fmt}{reset}"),
            logging.ERROR: logging.Formatter(f"‣ {red}{fmt}{reset}"),
            logging.CRITICAL: logging.Formatter(f"‣ {red}{bold}{fmt}{reset}"),
        }

        super().__init__(fmt, *args, **kwargs)

    def format(self, record: logging.LogRecord) -> str:
        return self.formatters[record.levelno].format(record)


def run(cmd: Sequence[str], *args: Any, **kwargs: Any) -> subprocess.CompletedProcess:
    try:
        return subprocess.run(cmd, *args, **kwargs, check=True, text=True)
    except FileNotFoundError:
        die(f"{cmd[0]} not found in PATH.")
    except subprocess.CalledProcessError as e:
        logging.error(f"\"{' '.join(str(s) for s in cmd)}\" returned non-zero exit code {e.returncode}.")
        raise e


def die(message: str) -> NoReturn:
    logging.error(message)
    sys.exit(1)


def do_cd(args: argparse.Namespace) -> None:
    logging.info("Setting up packaging repo")

    run(["git", "clone", "-b", f"c{args.release}s-sig-hyperscale", RPM_REPO, "rpm"])
    run(["git", "clone", SYSTEMD_REPO, "systemd", "--depth=1"])

    for line in (Path("systemd") / "meson.build").read_text().splitlines():
        if "version : " in line:
            version = re.sub("[^0-9]", "", line)  # Remove non-numeric characters
            break
    else:
        die("No version found in meson.build")

    # Make sure we always take precedence over any regular versions.
    version = f"{version}.999"

    short_commit = run(
        [
            "git",
            "rev-parse",
            "--short",
            "HEAD",
        ],
        cwd="systemd",
        stdout=subprocess.PIPE,
    ).stdout.strip()

    logging.info("Fixing up the specfile")
    # the date is to ensure the release is always monotonically increasing e.g. 1.20190320.4bf953d
    d = datetime.now()
    release = f"1.{d.strftime('%Y%m%d.%H%M%S')}.{short_commit}"

    changelog = f"{d.strftime('%a %b %d %Y')} {AUTHOR} - {version}-{release}"

    run(
        [
            "sed",
            "-i",
            "rpm/systemd.spec",
            # The meson rpm macros use --auto-features=enabled by default, which
            # means that every new meson feature introduced upstream breaks the
            # CI build. Let's override this so new features don't break the
            # build.
            "-e",
            r"1 i\%global __meson_auto_features auto",
            "-e",
            "/^Patch:.*/d",
            "-e",
            "/^Patch00.*/d",
            "-e",
            "/^Patch01.*/d",
            "-e",
            "0,/Source0: /s/^Source0: .*$/Source0:        systemd.tar.gz/",
            "-e",
            f"0,/Version: /s/^Version: .*$/Version:        {version}/",
            "-e",
            f"0,/Release: /s/^Release: .*$/Release:        {release}%{{?dist}}/",
            "-e",
            f"/%changelog/a * {changelog}\\n- Auto build from {short_commit}\\n",
        ]
    )

    run(["git", "--no-pager", "diff"], cwd="rpm")

    # Fix the systemd-user patch to apply cleanly on latest upstream.
    with (Path("rpm") / "fedora-use-system-auth-in-pam-systemd-user.patch").open("r+") as f:
        lines = f.readlines()
        lines.insert(26, " session  optional   pam_umask.so silent\n")
        lines.pop(23)
        f.seek(0)
        f.writelines(lines)

    prefix = "hs+fb" if args.repo == "facebook" else "hs"

    # For some reason the pagure archives are missing the 'v' prefix so we account for that.
    logging.info(f"Building tar file systemd.tar.gz")
    run(
        [
            "git",
            "archive",
            "--format=tar.gz",
            "-o",
            f"../rpm/systemd.tar.gz",
            # pagure removes the '+' from 'hs+fb' for the top level dir in the archive so we have to make
            # sure we do the same here.
            f"--prefix=systemd-{prefix.replace('+', '')}-{version}/",
            "HEAD",
        ],
        cwd="systemd",
    )

    logging.info("Building src.rpm")
    rpmbuild = run(
        [
            "rpmbuild",
            "--define",
            "_sourcedir .",
            *(["--define", "%facebook 1"] if args.repo == "facebook" else []),
            "-bs",
            "systemd.spec",
        ],
        cwd="rpm",
        stdout=subprocess.PIPE,
    )

    srcrpm = run(
        ["awk", "/Wrote:/ { print $2 }"],
        input=rpmbuild.stdout,
        stdout=subprocess.PIPE,
    ).stdout.strip()
    srcrpm = Path("rpm") / srcrpm
    logging.info(f"Wrote: {srcrpm}")

    suffix = "" if args.release == 8 else "s"

    run(
        [
            "cbs",
            *(["--cert", args.cert] if args.cert else []),
            "build",
            "--wait",
            "--fail-fast",
            "--skip-tag",
            f"hyperscale{args.release}s-packages-{args.repo}-el{args.release}{suffix}",
            str(srcrpm),
        ],
    )

    if not args.publish:
        logging.info("Publishing not requested, not tagging builds in testing")
        return

    run(
        [
            "cbs",
            *(["--cert", args.cert] if args.cert else []),
            "tag-build",
            f"hyperscale{args.release}s-packages-{args.repo}-testing",
            f"systemd-{version.replace('v', '')}-{release}.{prefix}.el{args.release}",
        ]
    )


@contextlib.contextmanager
def chdir(directory: Path) -> Iterator[None]:
    old = Path.cwd()

    if old == directory:
        yield
        return

    try:
        os.chdir(directory)
        yield
    finally:
        os.chdir(old)


class Verb(enum.Enum):
    cd = "cd"

    def __str__(self) -> str:
        return self.value

    def run(self, args: argparse.Namespace) -> None:
        with tempfile.TemporaryDirectory(dir="/var/tmp", prefix=f"systemd-releng-{self}") as d:
            with chdir(Path(d)):
                {Verb.cd: do_cd}[self](args)


def main() -> None:
    handler = logging.StreamHandler(stream=sys.stderr)
    handler.setFormatter(LogFormatter())
    logging.getLogger().addHandler(handler)
    logging.getLogger().setLevel("INFO")

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--release",
        help="CentOS Stream release to use (e.g 9)",
        metavar="RELEASE",
        default=9,
        choices=[8, 9],
        type=int,
    )
    parser.add_argument(
        "--repo",
        help="Hyperscale repository to build against",
        choices=["main", "facebook"],
        default="main",
    )
    parser.add_argument(
        "--cert",
        help="Path to the CentOS certificate to use",
        metavar="PATH",
        type=Path,
        default=None,
    )
    parser.add_argument(
        "--publish",
        action="store_true",
        help="Publish results of operation (by default only a dry-run is done)",
    )
    parser.add_argument(
        "verb",
        type=Verb,
        choices=list(Verb),
        help=argparse.SUPPRESS,
    )

    args = parser.parse_args()

    if args.cert:
        args.cert = args.cert.absolute()

    try:
        args.verb.run(args)
    except SystemExit as e:
        sys.exit(e.code)
    except KeyboardInterrupt:
        logging.error("Interrupted")
        sys.exit(1)
    except subprocess.CalledProcessError as e:
        sys.exit(e.returncode)


if __name__ == "__main__":
    main()
